import credentials from "../fixtures/credentials";
import request from "../utils/request";

test("/api/login with incorrect credentials", async () => {
  let response = await request({
    method: "POST",
    url: "https://sprinkle-burn.glitch.me/api/login",
    body: credentials.invalid
  });
  expect(response.statusCode).toBe(401)
  expect(response.body).toEqual({"errors":[{"title":"Credentials are incorrect"}]})
});

test("/api/login with correct credentials", async () => {
  let response = await request({
    method: "POST",
    url: "https://sprinkle-burn.glitch.me/api/login",
    body: credentials.valid
  });
  expect(response.statusCode).toBe(200)
  expect(response.body).toEqual({"data":{"username":"Dr I Test"}})
});
