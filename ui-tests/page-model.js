import { Selector } from "testcafe";

const loginPage = {
    usernameField: Selector('input[type=email]'),
    passwordField: Selector('input[type=password]'),
    loginButton:Selector('button').withText('Login')
};

export {loginPage};