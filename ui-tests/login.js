import { Selector, t } from "testcafe";
import credentials from "../fixtures/credentials";
import { loginPage } from "./page-model.js"

fixture`Login Page`.page`https://sprinkle-burn.glitch.me/`;

test("Can not login with incorrect credentials", async t => {

  await t
    .typeText(loginPage.usernameField,credentials.invalid.email)
    .typeText(loginPage.passwordField,credentials.invalid.password)
    .click(loginPage.loginButton)
    .expect(Selector("div#login-error-box").innerText).eql("Credentials are incorrect");
});

test("Can login with correct credentials", async t => {

  await t
    .typeText(loginPage.usernameField,credentials.valid.email)
    .typeText(loginPage.passwordField,credentials.valid.password)
    .click(loginPage.loginButton)
    .expect(Selector("article").innerText).contains("Welcome Dr I Test");
});
